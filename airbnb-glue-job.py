import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.dynamicframe import DynamicFrameCollection
from awsglue.dynamicframe import DynamicFrame
from awsglue.job import Job
import pyspark.sql.functions as F
from pyspark.sql.window import Window
from pyspark.sql import SQLContext

## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])


# source data
BUCKET_INPUT = 's3://my-airbnb-bucket/source_data/'
BUCKET_OUTPUT = 's3://my-airbnb-bucket/output_data/'

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)


# purge data
glueContext.purge_s3_path( f"{BUCKET_OUTPUT}/listings/", { "retentionPeriod": 0, "manifestFilePath": f"{BUCKET_OUTPUT}/mainifests/" } ) 
glueContext.purge_s3_path( f"{BUCKET_OUTPUT}/calendars/", { "retentionPeriod": 0, "manifestFilePath": f"{BUCKET_OUTPUT}/mainifests/" } ) 

# read listings to dynamic frame
listing_df = glueContext.create_dynamic_frame.from_options(
    format_options={"quoteChar": '"', "withHeader": True, "separator": ","},
    connection_type="s3",
    format="csv",
    connection_options={
        "paths": [f"{BUCKET_INPUT}/listings/"],
        "recurse": True,
    },
    transformation_ctx="Airbnb_datasource0",
)

# print schema
listing_df.printSchema()

# read listings to dynamic frame
calendar_df = glueContext.create_dynamic_frame.from_options(
    format_options={"quoteChar": '"', "withHeader": True, "separator": ","},
    connection_type="s3",
    format="csv",
    connection_options={
        "paths": [f"{BUCKET_INPUT}/calendars/"],
        "recurse": True,
    },
    transformation_ctx="Airbnb_datasource0",
)

# print schema
calendar_df.printSchema()


#cleaning

listings = listing_df.toDF()
calendars = calendar_df.toDF()

# preparing and cleaning data
cleaned_listings = listings.na.drop(subset=["host_id","id","room_type"])
cleaned_listings = cleaned_listings.filter(F.col("id").cast("int").isNotNull())
cleaned_listings = cleaned_listings.withColumn('listing_price', F.regexp_replace('price', '[$,]', '').cast('double'))
cleaned_listings = cleaned_listings.select("id","name","neighbourhood","latitude","longitude","room_type","listing_price")
# cleaned_listings = cleaned_listings.orderBy(F.col("id").asc()).limit(20)

cleaned_calendars = calendars.na.drop(subset=["listing_id","price","adjusted_price"])
cleaned_calendars = cleaned_calendars.withColumn('cal_price', F.regexp_replace('price', '[$,]', '').cast('double'))
cleaned_calendars = cleaned_calendars.withColumn('cal_date', F.to_date(F.col("date"),"yyyy-MM-dd").cast('date'))
cleaned_calendars = cleaned_calendars.withColumn('minimum_nights', F.col("minimum_nights").cast('int'))
cleaned_calendars = cleaned_calendars.withColumn('maximum_nights', F.col("maximum_nights").cast('int'))
cleaned_calendars = cleaned_calendars.select("listing_id","cal_date","available","cal_price","minimum_nights","maximum_nights")
# cleaned_calendars = cleaned_calendars.orderBy(F.col("listing_id").asc()).limit(20)
Window_Spec  = Window.partitionBy("listing_id").orderBy("cal_date")
cleaned_calendars = cleaned_calendars.withColumn('id', F.row_number().over(Window_Spec))

cleaned_calendars_df = DynamicFrame.fromDF(
        cleaned_calendars,
        glueContext,
        "Airbnb_datasource0",
)

cleaned_listings_df = DynamicFrame.fromDF(
        cleaned_listings,
        glueContext,
        "Airbnb_datasource0",
)

# using getCatalogSink to write a new table in the s3 location
listing_sink = glueContext.getSink(
        connection_type="s3", 
        path=f"{BUCKET_OUTPUT}listings/",
        enableUpdateCatalog=True
)

calendar_sink = glueContext.getSink(
        connection_type="s3", 
        path=f"{BUCKET_OUTPUT}calendars/",
        enableUpdateCatalog=True
)
# https://docs.aws.amazon.com/glue/latest/dg/aws-glue-programming-etl-format.html#aws-glue-programming-etl-format-parquet
listing_sink.setFormat("parquet", useGlueParquetWriter=True)
listing_sink.setCatalogInfo(catalogDatabase="default", catalogTableName="airbnb_listings")

calendar_sink.setFormat("parquet", useGlueParquetWriter=True)
calendar_sink.setCatalogInfo(catalogDatabase="default", catalogTableName="airbnb_calendars")

# write to glue table
listing_sink.writeFrame(cleaned_listings_df)
calendar_sink.writeFrame(cleaned_calendars_df)

# writing listing1 into mysql after data ingestion
listing_datasource = glueContext.create_dynamic_frame.from_catalog(database = "default", table_name = "airbnb_listings", transformation_ctx = "listing_datasource")

listing_applymapping = ApplyMapping.apply(frame = listing_datasource, mappings = [("id", "string", "id", "string"), ("name", "string", "name", "string"), ("neighbourhood", "string", "neighbourhood", "string"), ("latitude", "string", "latitude", "string"), ("longitude", "string", "longitude", "string"), ("room_type", "string", "room_type", "string"), ("listing_price", "double", "listing_price", "double")], transformation_ctx = "listing_applymapping")

listings_resolvechoice = ResolveChoice.apply(frame = listing_applymapping, choice = "make_cols", transformation_ctx = "listings_resolvechoice")

listing_dropnullfields = DropNullFields.apply(frame = listings_resolvechoice, transformation_ctx = "listing_dropnullfields")

listing_datasink = glueContext.write_dynamic_frame.from_jdbc_conf(frame = listing_dropnullfields, catalog_connection = "airbnb-rds", connection_options = {"dbtable": "listings1", "database": "airbnb", "preactions": "truncate listings1;"}, transformation_ctx = "listing_datasink")

# writing calendars1 into mysql after data ingestion
calendar_datasource = glueContext.create_dynamic_frame.from_catalog(database = "default", table_name = "airbnb_calendars", transformation_ctx = "calendar_datasource")

calendar_applymapping = ApplyMapping.apply(frame = calendar_datasource, mappings = [("id", "int", "id", "int"),("listing_id", "string", "listing_id", "string"), ("cal_date", "date", "cal_date", "date"), ("available", "string", "available", "string"), ("cal_price", "double", "cal_price", "double"), ("minimum_nights", "int", "minimum_nights", "int"), ("maximum_nights", "int", "maximum_nights", "int")], transformation_ctx = "calendar_applymapping")

calendar_selectfields = SelectFields.apply(frame = calendar_applymapping, paths = ["listing_id", "cal_date", "available", "cal_price", "minimum_nights", "maximum_nights"], transformation_ctx = "calendar_selectfields")
# de-duplication using MATCH_CATALOG
calendar_resolvechoice1 = ResolveChoice.apply(frame = calendar_selectfields, choice = "MATCH_CATALOG", database = "default", table_name = "airbnb_calendars", transformation_ctx = "calendar_resolvechoice1")

calendar_resolvechoice2 = ResolveChoice.apply(frame = calendar_resolvechoice1, choice = "make_struct", transformation_ctx = "calendar_resolvechoice2")

calendar_dropnullfields = DropNullFields.apply(frame = calendar_resolvechoice2, transformation_ctx = "calendar_dropnullfields")

calendar_datasink = glueContext.write_dynamic_frame.from_jdbc_conf(frame = calendar_dropnullfields, catalog_connection = "airbnb-rds", connection_options = {"dbtable": "calendars1", "database": "airbnb", "preactions": "truncate calendars1;"}, transformation_ctx = "calendar_datasink")

job.commit()
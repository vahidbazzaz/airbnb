import sys
import boto3
from botocore.exceptions import ClientError
import json
import logging
import pymysql
import os

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


print("Trying to connect...")
logger.debug('The script is starting.')

try:
    conn = pymysql.connect(
        host=os.environ['db_host'],
        user=os.environ['db_user'],
        passwd=os.environ['db_pass'],
        db=os.environ['db_name'],
    )
except pymysql.MySQLError as e:
    print("ERROR: Unexpected error: Could not connect to MySQL instance.")
    print(e)
    raise
except ClientError as e:
    print("ERROR: Unexpected error: Could not get secret from secret manager.")
    print(e)
    raise

def lambda_handler(event, context):
    
    queries = [
        """
    CREATE TABLE IF NOT EXISTS listings1 (
        id VARCHAR(50) PRIMARY KEY, 
        name VARCHAR(90), 
        neighbourhood VARCHAR(65),
        latitude DOUBLE,
        longitude DOUBLE,
        room_type VARCHAR(25),
        listing_price DOUBLE
    )""",
        """
    CREATE TABLE IF NOT EXISTS calendars1 (
        calendar_id BIGINT AUTO_INCREMENT PRIMARY KEY,
        listing_id VARCHAR(50) NOT NULL,
        cal_date DATE,
        available ENUM('t', 'f'),
        cal_price DOUBLE,
        minimum_nights INT,
        maximum_nights INT
    )""",
    """TRUNCATE calendars1""",
    """TRUNCATE listings1""",
    ]

    total = 0
    result = None
    print("Trying to create and truncate tables ...")
    logger.debug('Trying to create and truncate tables ...')
    with conn.cursor() as cur:
        for query in queries:
            cur.execute(query)
            total += 1
        conn.commit()
        print("created and truncated tables ...")
        logger.debug('created and truncated tables ...')
    
        
    # TODO implement
    return {
        'statusCode': 200,
        'body': json.dumps('DB Successfully Cleaned Up!'),
        'event': json.dumps(event)
    }
